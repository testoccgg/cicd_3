import sys
import io

from app import main


def test_main_pattern_output(login):
    capturedOutput = io.StringIO()
    sys.stdout = capturedOutput
    result = main()
    sys.stdout = sys.__stdout__
    app_output = capturedOutput.getvalue()

    loginExist = app_output.index(f"You login") > -1
    passExist = app_output.index(f"Don't forget password") > -1
    if (loginExist == -1 | passExist == -1):
        assert False, "bad app output, not match pattern"
    assert True


def test_main_login_output(login):
    capturedOutput = io.StringIO()
    sys.stdout = capturedOutput
    result = main()
    sys.stdout = sys.__stdout__
    app_output = capturedOutput.getvalue()
    passPart = app_output.index('Don\'t')
    if (passPart == -1):
        assert False, " console not by pattern"
    app_output = app_output[0:passPart]
    assert app_output.index(f"You login - {login}") > -1


def test_main_password_safely_way(passw):
    capturedOutput = io.StringIO()
    sys.stdout = capturedOutput
    result = main()
    sys.stdout = sys.__stdout__
    app_output = capturedOutput.getvalue()

    passwGoodOutput = app_output.index(f"Don't forget password - {passw}")
    if (passwGoodOutput == -1):
        assert False, "password outpu incorrect"
    assert True
