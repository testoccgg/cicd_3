from pytest import fixture


def pytest_addoption(parser):
    parser.addoption("--login", action="store", default="")
    parser.addoption("--passw", action="store", default="")


@fixture()
def login(request):
    return request.config.getoption("--login")


@fixture()
def passw(request):
    return request.config.getoption("--passw")
