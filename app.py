"""For CICD app"""
import os  # pylint: disable=import-error


def main():
    """Main function"""
    print(f"You login - {os.environ['MyLogin']} \r\nDon't forget password - {os.environ['MyPassword']} ")


if __name__ == '__main__':
    main()
